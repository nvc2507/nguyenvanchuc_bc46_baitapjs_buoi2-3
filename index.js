//bài tập 1
function tinhLuong() {
    var luong1Ngay = document.getElementById('luong1Ngay').value ;
    var soNgayLam = document.getElementById('soNgayLam').value ;
    var tongLuong = 0;
    tongLuong = luong1Ngay * soNgayLam;
    document.getElementById('tongLuong').innerHTML = tongLuong.toLocaleString();
}

//bài tập 2

function trungBinhCong() {
    var num1 = document.getElementById('num1').value *1 ;
    var num2 = document.getElementById('num2').value *1 ;
    var num3 = document.getElementById('num3').value *1 ;
    var num4 = document.getElementById('num4').value *1 ;
    var num5 = document.getElementById('num5').value *1 ;
    
    var trungBinh = 0;
    trungBinh = (num1 + num2 + num3 + num4 +num5) / 5;
    
    
    document.getElementById('trungBinh').innerHTML = trungBinh;
}

//bài tập 3

function quyDoiTien() {
    var Usd = 23500;
    var soLuongUSD = document.getElementById('soLuongUSD').value * 1;
    var tienQuyDoi = 0;

    tienQuyDoi = Usd * soLuongUSD;
    document.getElementById('tienQuyDoi').innerHTML = tienQuyDoi.toLocaleString();
}

// bài tập 4 

function phepTinh() {
    var chieuDai = document.getElementById('chieuDai').value *1;
    var chieuRong = document.getElementById('chieuRong').value *1;
    var dienTich = 0;
    var chuVi = 0;
    dienTich = chieuDai * chieuRong;
    chuVi = (chieuDai + chieuRong) * 2;
    document.getElementById('ketQua').innerHTML = `Diện tích : ${dienTich} : Chu Vi ${chuVi}`;
}

// Bài tập 5:

function tongKySo() {
    var num = document.getElementById('number').value;
    var hangDonVi = 0;
    var hangChuc = 0;
    var tong2KySo = 0;
    hangDonVi = Math.floor(num % 10);
    hangChuc = Math.floor(num / 10);
    tong2KySo = hangDonVi + hangChuc;
    document.getElementById('tong2KySo').innerHTML = tong2KySo;
}